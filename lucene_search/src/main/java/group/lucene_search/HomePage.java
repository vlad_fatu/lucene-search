package group.lucene_search;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.analysis.ro.RomanianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePage(final PageParameters parameters) {

		RomanianAnalyzer romAnalyzer = new RomanianAnalyzer(Version.LUCENE_35);
		// 1. create the index
		Directory index = new RAMDirectory();

		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_35,
				romAnalyzer);

		IndexWriter w;
		try {
			w = new IndexWriter(index, config);
			addDoc(w, "Lucene in Action român");
			addDoc(w, "Lucene for Dummies ");
			addDoc(w, "Managing Gigabytes românii mamelor");
			addDoc(w, "The Art of Computer Science român");
			w.close();
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LockObtainFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		final TextField<String> searchField = new TextField<String>(
				"searchField", Model.of(""));
		searchField.setRequired(true);

		Form<?> form = new Form<Void>("searchForm") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {

				String searchFieldValue = searchField.getModelObject();

				PageParameters pageParameters = new PageParameters();
				pageParameters.set("searchField", searchFieldValue);
				setResponsePage(HomePage.class, pageParameters);

			}

		};

		add(form);
		form.add(searchField);

		// 2. query
		String querystr;
		ArrayList<String> list = new ArrayList<String>();
		// if(parameters.containsKey("searchField")){
		querystr = parameters.get("searchField").toString();
		// }
		// else
		if (querystr != null) {

			// 3. search
			int hitsPerPage = 10;

			try {
				Query q = new QueryParser(Version.LUCENE_35, "title",
						romAnalyzer).parse(querystr);

				IndexReader reader = IndexReader.open(index);
				IndexSearcher searcher = new IndexSearcher(reader);
				TopScoreDocCollector collector = TopScoreDocCollector.create(
						hitsPerPage, true);
				searcher.search(q, collector);
				ScoreDoc[] hits = collector.topDocs().scoreDocs;
				// 4. display results
				System.out.println("Found " + hits.length + " hits for " + querystr);
				for (int i = 0; i < hits.length; ++i) {
					int docId = hits[i].doc;
					Document d = searcher.doc(docId);
					System.out.println((i + 1) + ". " + d.get("title"));
					list.add((i + 1) + ". " + d.get("title"));
				}

				// searcher can only be closed when there
				// is no need to access the documents any more.
				searcher.close();
			} catch (CorruptIndexException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		ListView<String> listview = new ListView<String>("listview", list) {
			private static final long serialVersionUID = 1L;

			protected void populateItem(ListItem<String> item) {
				item.add(new Label("result", item.getModel()));
			}
		};
		add(listview);

	}

	private static void addDoc(IndexWriter w, String value) throws IOException {
		Document doc = new Document();
		doc.add(new Field("title", value, Field.Store.YES, Field.Index.ANALYZED));
		w.addDocument(doc);
	}
}
